package com.mc.test;

import com.mc.core.Context;

public class Main {

    public static void main(String[] args) {
        Context context = new Context(Context.Choice.SUB);
        int result = context.operate(6, 5);
        System.out.println(result);

        Context context2=new Context(Context.Choice.ADD);
        int result2=context2.operate(6,5);
        System.out.println(result2);

    }
}
