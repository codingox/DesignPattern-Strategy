package com.mc.core;

/**
 * Created by LCF on 2016/1/25.
 * 功能：加法策略的具体实现类。
 * 说明：Java中类不加任何修饰符是默认状态，此情况只允许在同一个包访问，之所以这么设计是为了减少暴露不必要变量的风险。
 * 观点：个人认为对于类或者方法：能不暴露更多的就不暴露更多的。
 */
class AddStrategy implements Strategy {
    @Override
    public int operate(int a, int b) {
        return a+b;
    }
}
