package com.mc.core;

/**
 * Created by LCF on 2016/1/25.
 * 上下文对象实现类，有承上启下的作用。
 */
public class Context {
    /**
     * 限制用户调用无参构造方法
     */
    private Context(){}

    /**
     * 策略的公共接口类
     */
    private Strategy strategy;

    /**
     * 枚举类型限制选择。
     */
    public enum Choice{
        ADD,SUB
    }

    /**
     * 构造方法为确定使用哪种策略
     * @param choice 具体策略的映射
     */
    public Context(Choice choice){
        if ("ADD".equals(choice.toString())) {
            strategy=new AddStrategy();
        }else{
            strategy=new SubStrategy();
        }
    }

    /**
     * 本质为调用公共接口类的方法。
     * @param a 运算所需第一个参数
     * @param b 运算所需第二个参数
     * @return
     */
    public int operate(int a,int b){
        return strategy.operate(a,b);
    }
}
